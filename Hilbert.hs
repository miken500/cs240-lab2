module Hilbert where

rotation_table = [3, 0, 0, 1]
sense_table = [-1, 1, 1, -1]
quad_table = [[[0,1],[3,2]],[[1,2],[0,3]],
              [[2,3],[1,0]],[[3,0],[2,1]]]
hilbert side orig_x orig_y =
        let hilbert_helper x y sense rotation num k = 
                let xbit = x `div` k
                    ybit = y `div` k
                    newX = x - k*xbit
                    newY = y - k*ybit
                    newQuad = quad_table !! rotation !! xbit !! ybit
                    newNum  = num + if sense == -1 then 
                                        k*k*(3-newQuad)
                                    else k*k*newQuad
                    newRotation = rotation + (rotation_table !! newQuad)
                    modRotation = if newRotation >= 4 then 
                                        newRotation - 4
                                  else newRotation
                    newSense = sense * (sense_table !! newQuad) in
                if (k `div` 2) > 0 then
                        hilbert_helper newX newY newSense modRotation newNum (k `div` 2)
                else
                        newNum
        in hilbert_helper orig_x orig_y 1 0 0 (side `div` 2)
