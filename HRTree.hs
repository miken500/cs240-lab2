module Main where
import Hilbert
import Data.Tree
import System.IO
import Data.List
side = 65536 
type Mbr = Rect
data NodeData = Leaf Rect | NonLeaf Mbr deriving (Show)  

data Rect = Rect { hpv :: Int, lowX :: Int, lowY :: Int,
		   highX :: Int, highY :: Int } deriving (Show)
findMBR :: [Rect] -> Rect
findMBR lst =
	let foldHelper (Rect _ minX minY maxX maxY) (Rect _ lowX lowY highX highY) = 
		(Rect 0 (minimum (minX:lowX:highX:[]))
		        (minimum (minY:lowY:highY:[]))
		        (maximum (maxX:highX:lowX:[]))
		        (maximum (maxY:highY:lowY:[]))) in
	foldl foldHelper (Rect 0 100000 100000 0 0) lst


wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s = case dropWhile p s of
			"" -> []
			s' -> w : wordsWhen p s''
				where (w,s'') = break p s'
wordsComma = wordsWhen (==',')

convertListToRect :: String -> Rect
convertListToRect line = 
	let lst = map read $ wordsComma line 
            hilbertnum = hilbert side (((lst !! 0) + (lst !! 2) + 
                                        (lst !! 4) + (lst !! 6)) `div` 4)
                                      (((lst !! 1) + (lst !! 3) + 
                                        (lst !! 5) + (lst !! 7)) `div` 4) in
	Rect hilbertnum (max (lst !! 4) (lst !! 6)) (max (lst !! 5) (lst !! 3))
	       (max (lst !! 0) (lst !! 2)) (max (lst !! 1) (lst !! 7))

readRectangleFile :: String -> IO [Rect]
readRectangleFile file = do
	fileContents <- readFile file
	return $ map convertListToRect (lines fileContents) 
	
chooseLeaf :: Rect -> Tree NodeData-> [Tree NodeData] ->[Tree NodeData]
chooseLeaf _ t@(Node (Leaf rect) _) path = t:path
chooseLeaf r@(Rect hpv _ _ _ _) t@(Node (NonLeaf mbr) nodes) path = 
        let findHelper (Node (NonLeaf (Rect lhv _ _ _ _)) _) = lhv >= hpv
	    findHelper (Node (Leaf (Rect lhv _ _ _ _)) _) = lhv >= hpv
	    childNode = error "chooseLeaf failed" `maybe` id $
          		  (find findHelper nodes)
	in chooseLeaf r childNode (t:path) 
        
main = readRectangleFile "rects.txt" >>= return . take 4
testForest =  [Node (NonLeaf (Rect 2 3 5 35 40)) leafs1, 
               Node (NonLeaf (Rect 4 20 38 55 75)) leafs2,
               Node (NonLeaf (Rect 6 50 10 80 40)) leafs3]
leafs1 = [Node (Leaf (Rect 1 1 1 1 1)) [], Node (Leaf (Rect 2 2 2 2 2)) []]
leafs2 = [Node (Leaf (Rect 3 1 1 1 1)) [], Node (Leaf (Rect 4 2 2 2 2)) []]
leafs3 = [Node (Leaf (Rect 5 1 1 1 1)) [], Node (Leaf (Rect 6 2 2 2 2)) []]

 
