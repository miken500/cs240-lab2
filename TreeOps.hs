import Data.Tree
import Data.List
data NodeData a = Leaf a [a] | NonLeaf a deriving (Show)
--data Tree = Node a [Tree a]

cl = 8 -- Number of vals per Leaf
cn = 8 -- Number of Leafs/Nonleafs per NonLeaf
checkEmpty :: Tree (NodeData a) -> Bool
checkEmpty (Node _ []) = True
checkEmpty (Node _ _) = False

childrenLeafs :: Tree (NodeData a) -> Bool
childrenLeafs (Node _ ((Node (Leaf _ _) _):rest)) = True
childrenLeafs _ = False

chooseLeaf' :: (Ord a) => a -> Tree (NodeData a) 
                            -> [Tree (NodeData a)] 
                            -> [Tree (NodeData a)]
--chooseLeaf valToInsert root path -> path
chooseLeaf' val n@(Node (Leaf _ _) []) path = n:path
chooseLeaf' val n@(Node (NonLeaf _) lst) path =
        let findHelper (Node (NonLeaf itmval) _) = itmval >= val
            findHelper (Node (Leaf itmval _) _) = itmval >= val
            childNode = (last lst) `maybe` id $ (find findHelper lst)
        in chooseLeaf' val childNode (n:path)

chooseLeaf :: (Ord a) => a -> Tree (NodeData a) -> [Tree (NodeData a)]
chooseLeaf val root = chooseLeaf' val root []

insertVal :: (Ord a) => a -> Tree (NodeData a) -> Tree (NodeData a)
insertVal val (Node (NonLeaf _) _) = error "Cannot insert val into NonLeaf" 
insertVal val (Node (Leaf prevVal lst) _) = 
	Node (Leaf (max val prevVal) $ insert val lst) []

getSiblings :: (Ord a) => [Tree (NodeData a)] -> Tree (NodeData a)
getSiblings ((Node (Leaf maxVal _) _):(Node _ lst):rest) = 
	last $ takeWhile helper lst
	where	helper (Node (Leaf val _) _) = val < maxVal
		helper (Node (NonLeaf val ) _) = val < maxVal  

insertLeaf :: (Ord a) => a -> Tree (NodeData a) -> Tree (NodeData a)
insertLeaf val root =
        case chooseLeaf val root of 
                n@(Node (Leaf _ lst) _):path -> 
                        if length lst >= cl then
                                error "overflow"
                        else
                                insertVal val n 
testTree1 = (Node (NonLeaf 1) [(Node (Leaf 1 [1]) []),(Node (Leaf 2 [2]) [])]) 
testTree2 = (Node (NonLeaf 1) [(Node (NonLeaf 2) []),(Node (NonLeaf 3) [])])
